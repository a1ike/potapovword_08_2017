$(document).ready(function () {

  $('.pw-banners').slick({
    arrows: false,
    dots: true,
    autoplay: true
  });

  $('.pw-reviews__cards').slick({
    arrows: true,
    nextArrow: '<img src="./images/icon_left.png" class="slick-prev">',
    prevArrow: '<img src="./images/icon_right.png" class="slick-next">',
    dots: true,
    autoplay: true
  });

  $('.pw-header__switcher').click(function () {
    $('.pw-header__nav').slideToggle('fast', function () {
      // Animation complete.
    });
  });

  $('.pw-collapse__head').click(function () {
    $(this).next().slideToggle('fast', function () {
      $(this).prev().find('.pw-collapse__head-arrow').toggleClass('pw-collapse__head-arrow_rotated');
    });
  });

});